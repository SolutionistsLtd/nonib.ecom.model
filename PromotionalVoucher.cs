﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class PromotionalVoucher
    {
        public string CustomerNumber { get; set; }
                
        public string VoucherCode { get; set; }
                
        public decimal VoucherAmount { get; set; }

        public string Currency { get; set; }

        public bool Valid { get; set; }
                
        public string ValidationReason { get; set; }
                
        public string VoucherType { get; set; }
                
        public string CampaignType { get; set; }
                
        public decimal AdditionalBonus { get; set; }
    }

    public class PromotionalVoucherRedeem
    {
        public string CustomerNumber { get; set; }
        public string VoucherCode { get; set; }
        public decimal Amount { get; set; }
        public BrandCode BrandCode { get; set; }
        public string OrderNumber { get; set; }

    }
}
