﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class StyleAttribute
    {
        public string AttributeCode { get; set; }
        public string AttributeLabel { get; set; }
        public string AttributeSetCode { get; set; }
        public string AttributeSetLabel { get; set; }
        public bool Exclusive { get; set; }
    }
}
