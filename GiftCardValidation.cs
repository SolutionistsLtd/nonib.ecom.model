﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class GiftCardValidation
    {
         public bool Valid { get; set; }

        /// <summary>
        /// Can be ECard or GiftCard. Has to be provided.
        /// 05 – Gift card, 06 – e-card
        /// </summary>
        
        public string GiftCardType { get; set; } //Need to get this based on the voucher numbers

        /// <summary>
        /// Gift Card Number
        /// </summary>
        
        public string GiftCardNo { get; set; }

        /// <summary>
        /// Gift Card 4 digit pin
        /// </summary>
        
        public string GiftCardPin { get; set; }

        /// <summary>
        /// The value remaining on the gift card
        /// </summary>
        
        public decimal? ValueRemaining { get; set; }

        public string Currency { get; set; }

        /// <summary>
        /// The date of expiry for the gift card
        /// </summary>

        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// 01 - Card Expired, 02 – Pin not valid, 03 – Card number does not exists
        /// </summary>
        
        public string VerificationReason { get; set; }

        /// <summary>
        /// Date Format = "yyyy-MM-dd HH:mm"
        /// </summary>
        
        public DateTime? LastRedeemDate { get; set; }
    }
}
