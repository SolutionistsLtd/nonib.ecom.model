﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class Sku
    {
        
        public decimal SkuId { get; set; }
        
        public string StyleCode { get; set; }
        
        public string ColourCode { get; set; }

        public string MasterColourCode { get; set; }

        /// <summary>
        /// The Colour description eg. Red
        /// </summary>

        public string ColourDescription { get; set; }
        /// <summary>
        /// The Style Colour description eg. Deep Red
        /// </summary>
        
        public string StyleColourShortDescription { get; set; }
        /// <summary>
        /// The Style Colour description eg. Deep Red
        /// </summary>
        
        public string StyleColourLongDescription { get; set; }
        
        public string SizeCode { get; set; }

        //For multi dimension product
        public string SecondarySizeCode { get; set; }

        public List<string> UpcNumber { get; set; }
        /// <summary>
        /// The number that can be used to sort sizes per style colour
        /// </summary>
        
        public int SizeSeqNo { get; set; }

        
        public DateTime? FirstAllocationDate { get; set; }

        public List<StyleRetail> StyleRetails { get; set; }
    }



}
