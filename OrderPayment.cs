﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class OrderPayment : IOrderPayment
    {
        /// <summary>
        /// List of values when setting this field Payment Method.
        /// </summary>

        public string PaymentMethod { get; set; } //MasterCard,Diners,Visa,AMEX,Debit,Paypal,GiftVoucher,Other

        public decimal Amount { get; set; }

        /// <summary>
        /// Credit Card(Masked) number or Gift Voucher number
        /// </summary>

        public string CardNumber { get; set; }

        /// <summary>
        /// Can be used for Gift Card Pin No or Masked CC Auth
        /// </summary>

        public string Authorisation { get; set; }

        /// <summary>
        /// Authorised or not.
        /// </summary>

        public string PaymentStatus { get; set; }

        /// <summary>
        /// Currency=AUD,USD...
        /// </summary>

        public string Currency { get; set; }
    }
}
