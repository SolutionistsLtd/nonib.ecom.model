﻿namespace NoniB.Ecom.Model
{
    public interface IOrderPayment
    {
        decimal Amount { get; set; }
        string Authorisation { get; set; }
        string CardNumber { get; set; }
        string Currency { get; set; }
        string PaymentMethod { get; set; }
        string PaymentStatus { get; set; }
    }
}