﻿namespace NoniB.Ecom.Model
{
    public enum InstanceStatus
    {
        Unchanged,
        Dirty,
        New,
        Deleted
    }

    public enum AddressType
    {
        Postal,
        Delivery,
        ParcelPoint
    }

    public enum ItemStatus
    {
        Received,
        SubmittedToWarehouse,
        Pick,
        Shipped,
        Cancelled
    }    

    public enum Headertatus
    {
        New,
        Hold,
        Cancelled
    }    


    public enum BrandCode
    {
        Rockmans = 2,
        BeMe = 3,
        WLane = 4,
        NoniB = 7,
        Millers = 8,
        Katies = 11,
        Autograph = 10,
        Rivers = 12,
        Crossroads = 9,
        CurveSociety = 13
    }

    /// <summary>
    /// Order Type enum
    /// </summary>
    public enum OrderType
    {
        /// <summary>
        /// New  == Sale,
        /// </summary>
        Sale,
        Return,
        Exchange
    }
    /// <summary>
    /// Order Channel enum
    /// </summary>
    public enum Channel
    {
        /// <summary>
        /// Single brand stock order
        /// </summary>
        Standard,
        /// <summary>
        /// Cross brand stock order
        /// </summary>
        CrossSelling,
        /// <summary>
        /// Click and Collect order
        /// </summary>
        ClickCollect,
        /// <summary>
        /// Channel Advisor order
        /// </summary>
        ChannelAdvisor
    }

       public enum UpdateSource
    {
        Online = 0,
        NewsletterRegistration = 1,
        MemberRegistration = 2,
        Checkout = 3,
        MyDetails = 4
    }
}
