﻿
namespace NoniB.Ecom.Model
{
    public class Colour
    { 
        public string ColourCode { get; set; }
        public string ColourLongDesc { get; set; }
        public string ColourShortDesc { get; set; }
        public bool Active { get; set; }
    }
}