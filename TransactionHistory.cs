﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class TransactionHeader
    {
        public Int64? Id  { get; set; }
        public string TransactionType  { get; set; }
        public Int64? TransactionRefId  { get; set; }
        public String TransactionNumber  { get; set; }
        /// <summary>
        /// Store Identifier
        /// </summary>
        public string  LocationCode  { get; set; }
        public string  RegisterNo  { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime? TransactionDateTime  { get; set; }
        public DateTime? TradingDate  { get; set; }
        public Decimal? TotalPrice  { get; set; }
        public Decimal? TotalTax  { get; set; }
        public Decimal? TotalQty  { get; set; }


        public bool GuestCheckout { get; set; }

        public List<TransactionItem> TransactionItems { get; set; }
        public List<TransactionTender> TransactionTenders { get; set; }
        
    }

    public class TransactionItem
    {
        public Int32 LineId  { get; set; }
        public string ItemType  { get; set; }
        public String POSDescription  { get; set; }
        public Decimal? ActualPrice  { get; set; }
        public Decimal? OriginalPrice  { get; set; }
        public Decimal? LinePrice  { get; set; }
        public Decimal? LinePriceExTax  { get; set; }
        public Decimal? Qty  { get; set; }
        public String StyleCode  { get; set; }
        public String ColourCode { get; set; }
        public String ColourDescription { get; set; }
        public String SizeCode  { get; set; }
        public String SkuId { get; set; }
        public String ItemReference  { get; set; }
        public Decimal? TaxAmt  { get; set; }
        public Decimal? LineDiscountAmount  { get; set; }
        [IgnoreDataMember]
        public Int64 TransactionId { get; set; }


    }

    public class TransactionTender
    {
        public Int32 LineId  { get; set; }        
        public string TenderCode  { get; set; }
        /// <summary>
        /// Cash
        /// Master Card/Visa
        /// Cheque
        /// Forfeit
        /// Credit Note
        /// Gift Voucher
        /// Account
        /// Layby
        /// Cust Order
        /// Transfer Deposit
        /// POS Credit        
        /// </summary>
        public string TenderName { get; set; }
        public Decimal TenderAmount  { get; set; }
        public String Reference  { get; set; }
        [IgnoreDataMember]
        public Int64 TransactionId { get; set; }
    }



}
