﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class Style
    {
        public string StyleCode { get; set; }
        public string HierarchyGroupCode { get; set; }
        public string HierarchyGroupDesc { get; set; }
        public string BrandCode { get; set; }

        /// <summary>
        /// 1=New, 2=Prelim, 3=Ordered, 4=Received
        /// </summary>

        public string StyleStatus { get; set; }

        /// <summary>
        /// Epicor Long Description
        /// </summary>

        public string LongDescription { get; set; }

        /// <summary>
        /// Epicor Short Description
        /// </summary>
        public string ShortDescription { get; set; }

        public bool ReplenishmentFlag { get; set; }

        public DateTime CreateDate { get; set; }

        public string SupplierName { get; set; }

        public string ProductType { get; set; }

        public decimal StyleAverageCost { get; set; }

        public bool ActiveFlag { get; set; }

        public List<StyleAttribute> Attributes { get; set; }
        public List<Sku> Skus { get; set; }
    }
}
