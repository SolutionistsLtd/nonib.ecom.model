﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    /// <summary>
    /// If the flags are null then ignore that flag or update/insert
    /// </summary>
    public class CustomerBrandFlag
    {
        public BrandCode BrandCode { get; set; }
        public bool? MailOptIn { get; set; }
        public bool? EMailOptIn { get; set; }
        public bool? SMSOptIn { get; set; }
        public bool? GuestFlag { get; set; }
        public bool? EmailSubscriptionFlag { get; set; } //If this is true, we ignore all other flags except Email
        
    }

 
}
