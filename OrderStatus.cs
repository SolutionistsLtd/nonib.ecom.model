﻿
namespace NoniB.Ecom.Model
{

    /// <summary>
    /// Databyte will push to the service bus queue with label "EcomOrderStatus"
    /// 
    /// </summary>
    public class OrderStatus
    {
        public string OrderNumber { get; set; }
        public string OrderReference { get; set; }
        /// <summary>
        /// The Line ID in SOL.
        /// </summary>
        public string OrderLineId { get; set; }

        public string SkuId { get; set; }

        /// <summary>
        /// Values you will pass back to Solutionists 'Pick' and 'Shipped'.
        /// Where an order has been highlighted in OMS to be in a pick run set status of all lines to 'Pick'. 
        /// Where order has been shipped pass back status 'shipped' and qty actually shipped by line.
        /// </summary>
        
        public ItemStatus Status { get; set; }

        /// <summary>
        /// This will be by line ID. 
        /// Where an order has been shipped pass back status 'shipped'’ and qty actually shipped by lineID.
        /// </summary>
        
        public int QuantityShipped { get; set; }
        /// <summary>
        /// Carrier
        /// </summary>
        public string ShippingCompany { get; set; }
        /// <summary>
        /// Carrier tracking ID
        /// </summary>
        public string TrackingReference { get; set; }

        /// <summary>
        /// Warehouse or potentially store in future the product was shipped from.
        /// </summary>
        public string LocationCode { get; set; }
        
    }
}
