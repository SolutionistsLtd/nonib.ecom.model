﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoniB.Ecom.Model
{
    public class OrderVoucher : IOrderVoucher
    {
        /// <summary>
        /// Can be ECard or GiftCard. Has to be provided.
        /// 05 – Gift card, 06 – e-card
        /// </summary>

        public string GiftCardType { get; set; }

        /// <summary>
        /// The Order Line ID in the E-commerce site when the physical card has been fulfilled and will be sent back.
        /// </summary>

        public string OrderLineId { get; set; }

        /// <summary>
        /// Sold Voucher number. A Voucher number cannot be provided for a physical gift card.
        /// </summary>

        public string GiftCardNo { get; set; }

        /// <summary>
        /// The 4 digit pin for the voucher.
        /// </summary>

        public string GiftCardPin { get; set; }

        /// <summary>
        /// Amount of the gift voucher
        /// </summary>

        public decimal GiftCardValue { get; set; }

        /// <summary>
        /// Expiry date of the voucher.
        /// </summary>
        
        public DateTime? ExpiryDate { get; set; }

       /// <summary>
        /// Gift message
        /// </summary>

        public string GiftMessage { get; set; }

        public string GiftEmail { get; set; }
        public string Name { get; set; }
    }
}
